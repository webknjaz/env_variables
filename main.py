import os
import dotenv

dotenv_path = os.path.join(os.path.dirname(__file__), '.env')

HOVAL_PROG_KEY = dotenv.get_key(dotenv_path, "HOVAL_PROG_KEY")
HOVAL_REBATE_KEY = dotenv.get_key(dotenv_path, "HOVAL_REBATE_KEY")
UNIT_NETTO_PRICE_DISCOUNT = float(
    dotenv.get_key(dotenv_path, "UNIT_NETTO_PRICE_DISCOUNT"))

print(HOVAL_PROG_KEY)
print(HOVAL_REBATE_KEY)
print(UNIT_NETTO_PRICE_DISCOUNT)
